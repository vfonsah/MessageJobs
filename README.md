# Jobs-React-CoreUI
App for corporate use in assigning task messages (jobs) and sending to recipients. 
Front end with React JS using Core UI dashboard for React.
# Comes with additional npm dependencies for React: saved in node_modules
Makes use of some extra node dependencies other than those that come with Core UI
Node Modules is also uploaded into this repo to help point in the direction as to where come dependencies and/or components are being called from.
# Extensions worth noting are the following:
JS Spinners: *react-spinners-kit, react-spinner-material, react-loading-overlay*
File upload handling: *express, multer, cors, nodemon*
'Cross-origin-request-service: *Cors-anywhere* - used headers of all pull and push requests to and from servers api
