export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW',
      },
    },
    {
      title: true,  name: 'Jobs',
    },
    {
      name: 'Tasks',  url: '/tasks',  icon: 'fa fa-tasks',
      children: [
        {
          name: 'List Jobs',
          url: '/tasks',
          icon: 'cui-list',
        },
        {
          name: 'Create Job',
          url: '/tasks/create',
          icon: 'icon-plus',
        }
      ]
    },
    {
      divider: true,
    },
    {
      title: true,
      name: 'Admin',
    },
    {
      name: 'Users',
      url: '/pages',
      icon: 'icon-star',
      children: [
        {
          name: 'Login',
          url: '/login',
          icon: 'icon-star',
        },
        {
          name: 'Register',
          url: '/register',
          icon: 'icon-star',
        },
        {
          name: 'User List',
          url: '/users',
          icon: 'icon-star',
        },
      ],
    },
    {
      divider: true,
    },
    {
      title: true,
      name: 'Extras',
    },
    {
      name: 'Pages',
      url: '/pages',
      icon: 'icon-star',
      children: [
        {
          name: 'Error 404',
          url: '/404',
          icon: 'icon-star',
        },
        {
          name: 'Error 500',
          url: '/500',
          icon: 'icon-star',
        },
      ],
    },
  ],
};
