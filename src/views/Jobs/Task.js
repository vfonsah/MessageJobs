import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardText,
  CardHeader,
  CardImg,
  CardFooter,
  Col,
  Label,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  Row,
  Table
} from "reactstrap";
import { AppSwitch } from "@coreui/react";
import Spinner from "react-spinner-material";
import LoadingOverlay from "react-loading-overlay";

class Task extends Component {
  constructor(props) {
    super(props);

    this.state = {
      jobInfo: null,
      jobDetails: null,
      isLoading: true,
      error: null,
      baseUrl:
        "https://cors-anywhere.herokuapp.com/http://85.90.245.31:8089/messagejob/"
    };
    this.fetchJob = this.fetchJob.bind(this);
    this.fetchDetails = this.fetchDetails.bind(this);
  }

  fetchDetails() {
    const handle = this.props.match.params.id;
    const jobUrl = this.state.baseUrl;

    console.log(`${jobUrl}${handle}/details`);

    fetch(`${jobUrl}${handle}/details`, {
      method: "get",
      headers: new Headers({
        "content-type": "application/json;charset=UTF-8",
        accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
      })
    })
      .then(response => response.json())
      .then(res =>
        this.setState({
          jobDetails: res,
          isLoading: false
        })
      )
      .catch(error =>
        this.setState({
          error: error,
          isLoading: false
        })
      );
  }

  fetchJob() {
    const handle = this.props.match.params;
    console.log(handle)
    // const jobUrl = this.state.baseUrl;
    this.setState({
      jobInfo: handle,
    })

    // fetch(`${jobUrl}${handle}`, {
    //   method: "get",
    //   headers: new Headers({
    //     "content-type": "application/json;charset=UTF-8",
    //     accept:
    //       "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
    //   })
    // })
    //   .then(response => response.json())
    //   .then(data =>
    //     this.setState({
    //       jobInfo: data,
    //       isLoading: false
    //     })
    //   )
    //   .catch(error =>
    //     this.setState({
    //       error: error,
    //       isLoading: false
    //     })
    //   );
  }

  componentDidMount() {
    this.fetchDetails();
    // this.fetchJob();
  }

  render() {
    const { jobInfo, jobDetails, isLoading, error } = this.state;

    return (
      <Fragment>
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          <Fragment>
            <JobTable jobDetails={jobDetails} />
            {/* <JobCard jobInfo={jobInfo} /> */}
          </Fragment>
        ) : (
          <Fragment>
            <div className="text-center">
              <h2>Fetching message data..</h2>
            </div>
            <LoadingOverlay
              active={true}
              styles={{
                overlay: base => ({
                  ...base,
                  background: "rgba(40, 250, 211, 0.5)",
                  animation: "grow",
                  role: "status"
                }),
                wrapper: {
                  width: "100px",
                  height: "100px"
                }
              }}
              spinner={
                <Spinner
                  size={230}
                  spinnerColor={"#28befa"}
                  spinnerWidth={60}
                  visible
                >
                  <span className="sr-only">Loading...</span>
                </Spinner>
              }
            />
          </Fragment>
        )}
      </Fragment>
    );
  }
}

const jobLink = "/tasks/";
const testImgUrl = "/testimage/blog-post-image-2.jpeg";

const TableHeader = () => {
  return (
    <thead>
      <tr className="bg-primary text-light text-center text-uppercase">
        <th>recipient #</th>
        <th>phone</th>
        <th>locale</th>
        <th>sent</th>
      </tr>
    </thead>
  );
};

const TableBody = props => {
  
  const rows = props.jobDetails.map((row, index) => {
    return (
      <tr key={index} className="text-center">
        <td>{index}</td>
        <td>{row.phone}</td>
        <td>{row.locale}</td>
        <td className={`bg-${row.sent ? "success" : "warning"}`}>
          {row.sent ? "true" : "false"}
        </td>
      </tr>
    );
  });

  return rows;
};

const JobTable = props => {
  const details = props.jobDetails.content;
  return (
    <Table responsive bordered hover>
      <TableHeader />
      <tbody>
        <TableBody jobDetails={details} />
      </tbody>
    </Table>
  );
};

const JobCard = props => {
  const job = props.jobInfo;
  return (
    <Card className="min-vh-100 border border-default">
      <Row className="no-gutters py-3">
        <Col xs="1" />
        <Col md="4" className="bg-secondary" border="light">
          <CardImg
            src={`${testImgUrl}`}
            alt={`${job.id}: Job image`}
            height="100h"
            width="100w"
            className="w-100 p-0 h-100 d-inline-block fluid thumbnail"
          />
        </Col>
        <Col md="6" color="light" className="mt-0">
          <CardHeader className="my-0 bg-primary py-2">
            <CardText className="text-uppercase text-center text-medium text-default">
              job message details
            </CardText>
          </CardHeader>
          <CardBody className="py-1">
            <Label text="white">
              <Badge color="dark">English</Badge>
            </Label>
            <div className="border border-light px-2 py-2">
              <CardText className="">{`${job.messageEn}`}</CardText>
            </div>
            <hr />
            <Label text="white">
              <Badge color="dark">French</Badge>
            </Label>
            <div className="border border-light px-2 py-2">
              <CardText className="">{`${job.messageFr}`}</CardText>
            </div>
          </CardBody>
          {/* </Col>
        <Col md="2" color="light" className="min-vh-80"> */}
          <CardFooter className="px-0 py-0">
            <ListGroup variant="flush">
              <ListGroupItemHeading className="my-0 bg-primary py-2">
                <CardText className="text-uppercase text-center text-medium text-default">
                  status
                </CardText>
              </ListGroupItemHeading>
              <ListGroupItem className={job.run ? "bg-success" : "bg-warning"}>
                <CardText>
                  <span className="mr-auto text-dark ml-1 mr-1"> Sent:</span>

                  <span className="text-center ml-5 text-uppercase">
                    {job.run ? "yes" : "no"}
                  </span>
                </CardText>
              </ListGroupItem>
              <ListGroupItem className={job.run ? "bg-success" : "bg-warning"}>
                <CardText>
                  <span className="mr-auto text-dark ml-1 mr-1">
                    Completed:
                  </span>

                  <span className="text-center ml-2 text-uppercase">
                    {job.completed ? "complete" : "not complete"}
                  </span>
                </CardText>
              </ListGroupItem>
              <ListGroupItem>
                <CardText>
                  <Link
                    to={{ pathname: `${jobLink}${job.id}/edit` }}
                    className="btn btn-info"
                  >
                    Edit Job
                  </Link>
                </CardText>
              </ListGroupItem>
            </ListGroup>
          </CardFooter>
        </Col>
        <Col xs="1" />
      </Row>
    </Card>
  );
};

export default Task;
