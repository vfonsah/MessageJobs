import React, { Component, useState } from "react";
import { Badge, Button } from "reactstrap";
import { Link } from "react-router-dom";

const TableHeader = () => {
  return (
    <thead>
      <tr className="bg-primary text-light text-center text-uppercase">
        <th>Job ID</th>
        <th>message-english</th>
        <th>message-french</th>
        {/**
        <th>type / source</th>
         <th>
          status
          <br />
          (run)
        </th>
        <th>
          status
          <br />
          (completed)
        </th> 
        */}
        <th colSpan="3">Actions</th>
      </tr>
    </thead>
  );
};

const JobType = type => {
  let row = "";
  switch (type) {
    case 1:
      row = "Image";
      break;
    case 2:
      row = "File";
      break;
    default:
      row = "Text";
  }
  return row;
};

const TableBody = props => {
  const rows = props.fieldsData[0].content.map(row => {
    const jobLink = "/tasks/" + row.id + "/";
    return (
      <tr key={row.id}>
        <td className="text-center">{row.id}</td>
        <td className="text-left">{row.messageEn}</td>
        <td className="text-left">{row.messageFr}</td>
        {/**
        <td className="text-center">{JobType(row.type)}</td>
         <td>
          <Badge color={row.run ? "success" : "warning"}>
            {row.run ? "Sent" : "Not sent"}
          </Badge>
        </td>
        <td>
          <Badge color={row.completed ? "success" : "warning"}>
            {row.completed ? "Complete" : "Incomplete"}
          </Badge>
        </td>  */}

        <td>
          <Link
            to={{
              pathname: `${jobLink}edit`,
              data: row
            }}
            className="btn btn-secondary"
          >
            Edit
          </Link>
        </td>
        <td>
          <Link
            to={{ pathname: `${jobLink}`, state: { fieldsData: row } }}
            className="btn btn-info"
          >
            View
          </Link>
        </td>
        <td>
          <Button
            onClick={() => props.removeField(row.id)}
            className="btn btn-danger"
          >
            Delete
          </Button>
        </td>
      </tr>
    );
  });

  return <tbody>{rows}</tbody>;
};

const TableDesc = props => {
  const state = {
    modal: false
  };
  return (
    <Link
      to={"/tasks/create"}
      className="btn btn-secondary ml-5 px-1 py-0"
      onClick={props.openModal}
    >
      <i className="fa fa-plus-circle mr-1" />
      Add Job
    </Link>
  );
};

class TasksTable extends Component {
  render() {
    const { fieldsData, removeField } = this.props;

    return <TableBody fieldsData={fieldsData} removeField={removeField} />;
  }
}

export { TasksTable, TableDesc, TableHeader };
