import React, { Component, Fragment, FileReader } from "react";

import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  Input,
  Label,
  Row,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from "reactstrap";

class UpdateJob extends Component {
  render() {
    return (
      <Fragment>
        <Row>
          <Col xs="2" />
          <Card lg="8">
            <CardHeader className="bg-primary py-2">
              <h2><strong>Edit Job</strong></h2>
            </CardHeader>
            <CardBody className="mr-auto">
              <Form className="py-1 px-3">
                <FormGroup>
                  <Input type="hidden" name="id" value="" />
                  <Input type="hidden" name="date" value="" />
                </FormGroup>
                <FormGroup>
                  <Label className="pull-left" htmlFor="message_en">
                    Message (ENG)
                  </Label>
                  <textarea
                    type="text"
                    name="message_en"
                    placeholder="enter message to send"
                    value=""
                    onChange=""
                    className="form-control"
                  />
                </FormGroup>
                <FormGroup>
                  <label>Message (FRE)</label>
                  <textarea
                    type="text"
                    name="message_fre"
                    placeholder="entrer le message à envoyer"
                    value=""
                    onChange=""
                    className="form-control"
                  />
                </FormGroup>
                <FormGroup row>
                  <Col lg="8" className="custom-file">
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText className="custom-file-label">
                          <i className="fa fa-file mr-1">|</i>
                          <i className="fa fa-file-excel-o ml-1 mr-5" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="file"
                        className="custom-file-input form-control-file"
                        accept=""
                        onChange=""
                        data-toggle="tool-tip"
                        title={"file types supported: \n"}
                      />
                    </InputGroup>
                  </Col>
                  <Col lg="4">
                    <Button
                      className="form-control-file"
                      position="right"
                      color="light"
                    >
                      Load File
                    </Button>
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Button
                    type="submit"
                    color="primary"
                    className="pull-left mr-5"
                    onClick=""
                  >
                    Submit
                  </Button>
                  <Button
                    color="danger"
                    className="ml-5 position-center"
                    onClick=""
                    data-dismiss="modal"
                  >
                    Cancel
                  </Button>
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
          <Col xs="2" />
        </Row>
      </Fragment>
    );
  }
}

export default UpdateJob;
