import React, { Component, Fragment, FileReader } from "react";

import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  Input,
  Label,
  Row,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from "reactstrap";

class CreateJob extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message_en: "",
      message_fre: "",
      date: [],
      recipientsList: null,
      id: "",
      fileLoaded: false
      //modal states
      //   modal: false,
      //   modalCreate: false
    };
    // this.toggle = this.toggle.bind(this);
  }

  //   toggle() {
  //     this.setState({
  //       modal: !this.state.modal
  //     });
  //   }

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  handleChosenFile = event => {
    console.log(event.target.files[0]);
    this.setState({
      recipientsList: event.target.files[0],
      fileLoaded: 0
    });
  };

  submitForm = event => {
    event.preventDefault();

    this.handleSubmit(this.state);
    /**
     * this.setState below is to reset the state of the form values once CREATE process is complete
     * use it as per your design requirements
     */
    this.setState(this.initialState);
  };

  filesSupported = () => {
    return [
      ".csv",
      ".docx",
      ".txt",
      ".xls",
      ".ods",
      ".xlsx",
      ".ots",
      ".html",
      ".xml",
      ".xlt",
      ".uos",
      ".fods",
      ".dif",
      ".dbf",
      ".xlms"
    ];
  };

  handleSubmit = field => {
    // this.preventDefault();
    this.setState({
      fieldsData: [this.state.fieldsData, field] //add the new state of data to the array fieldsData
    });
  };

  render() {
    // let fileReader;

    const { message_en, message_fre, date, recipients, id } = this.state;

    // const {fieldData} = this.props;
    const filesSupported = [
      " .csv",
      " .docx",
      " .txt",
      " .xls",
      " .ods",
      " .xlsx",
      " .ots",
      " .html",
      " .xml",
      " .xlt",
      " .uos",
      " .fods",
      " .dif",
      " .dbf",
      " .xlms"
    ];

    // const handleFileRead = e => {
    //   const content = fileReader.result;
    //   this.recipients = content;
    //   console.log(recipients);
    // };

    // const handleFileChosen = file => {
    //   fileReader = new FileReader();
    //   fileReader.onloadend = handleFileRead;
    //   fileReader.readAsText(file);
    // };

    return (
      <Fragment>
        <Row>
          <Col xs="12" sm="6" md="12" lg="12">
            <Card>
              <CardHeader className="bg-primary">
                  <strong>Create Job</strong>
                  <small> Form</small>
                  {/* <span row className="ml-auto pull-right">
                    <Input
                      type="button"
                      name="to-fre"
                      value="French"
                      className="btn btn-secondary py-0 px-1 mr-1"
                    />
                    <i className="cui-code" />
                    <Input
                      type="button"
                      name="to-eng"
                      value="English"
                      className="btn btn-secondary pull-right py-0 px-1 ml-1"
                    />
                  </span> */}
              </CardHeader>
              <div className="row no-gutters grid-divider">
                <CardBody className="mr-auto" lg="8">
                  <Form onSubmit={this.submitForm} className="py-1 px-3">
                    <FormGroup>
                      <Input type="hidden" name="id" value={id} />
                      <Input type="hidden" name="date" value={date} />
                    </FormGroup>
                    <FormGroup>
                      <Label className="pull-left" htmlFor="message_en">
                        Message (ENG)
                      </Label>
                      <textarea
                        type="text"
                        name="message_en"
                        placeholder="enter message to send"
                        value={message_en}
                        onChange={this.handleChange}
                        className="form-control"
                      />
                    </FormGroup>
                    <FormGroup>
                      <label>Message (FRE)</label>
                      <textarea
                        type="text"
                        name="message_fre"
                        placeholder="entrer le message à envoyer"
                        value={message_fre}
                        onChange={this.handleChange}
                        className="form-control"
                      />
                    </FormGroup>
                    <FormGroup row>
                      <Col lg="8" className="custom-file">
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText className="custom-file-label">
                              <i className="fa fa-file mr-1">|</i>
                              <i className="fa fa-file-excel-o ml-1 mr-5" />
                              {this.state.recipientsList
                                ? this.state.recipientsList.name
                                : "Recipients List"}
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="file"
                            className="custom-file-input form-control-file"
                            accept={this.filesSupported()}
                            onChange={this.handleChosenFile}
                            data-toggle="tool-tip"
                            title={"file types supported: \n" + filesSupported}
                          />
                        </InputGroup>
                      </Col>
                      <Col lg="4">
                        <Button
                          className="form-control-file"
                          position="right"
                          color="light"
                        >
                          Load File
                        </Button>
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <Button
                        type="submit"
                        color="primary"
                        className="pull-left mr-5"
                        onClick={this.submitForm}
                      >
                        Submit
                      </Button>
                      <Button
                        color="danger"
                        className="ml-5 position-center"
                        onClick={this.toggle}
                        data-dismiss="modal"
                      >
                        Cancel
                      </Button>
                    </FormGroup>
                  </Form>
                </CardBody>
                <CardBody className="ml-auto" lg="4">
                  <h3 className="text-center">Recipients List</h3>
                  <div className="min-h-100 min-w-100" />
                </CardBody>
              </div>
            </Card>
          </Col>
        </Row>
      </Fragment>
    );
  }
}
{
  /* <CreateJob handleSubmit={this.handleSubmit} openModal={this.openModal} /> */
}

export default CreateJob;
