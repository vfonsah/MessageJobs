import React, { Component, Fragment } from "react";
import Spinner from "react-spinner-material";
import { CubeSpinner, TraceSpinner, GuardSpinner } from "react-spinners-kit";
import LoadingOverlay from "react-loading-overlay";
import ReactPaginate from 'react-paginate'

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table
} from "reactstrap";
import { CreateJob, EditJob } from "./CreateJob";
import { TableDesc, TableHeader, TasksTable } from "./ListJobs";

class Tasks extends Component {
  constructor(props) {
    super(props);

    this.state = {
      /*
      sample data to be presented on the tasks table
      could be removed before passing data through (production mode)
      */
      isLoading: true,
      fieldsData: [
        {
          id: "1",
          messageEn: "welcome here",
          messageFr: "bienvenue ici",
          type: 1,
          run: false,
          completed: true,
          date: "today"
        },
        {
          id: "2",
          messageEn: "It is a new day, yes it is",
          messageFr: "C'est une journee nouveau, n'est pas",
          type: 1,
          completeted: true,
          date: "today",
          run: false
        },
        {
          id: "3",
          messageEn: "You all have class tomorrow",
          messageFr: "Vous vous avez les cours demain",
          type: 1,
          completeted: true,
          date: "today",
          run: false
        }
      ],
      error: null,
      baseUrl: "https://cors-anywhere.herokuapp.com/http://85.90.245.31:8089",
      // states for paging
      totalSize: 0,
      page: 1,
      sizePerPage: 10,
    };
  }

  fetchJobs() {
    const baseUrl = this.state.baseUrl;

    fetch(`${baseUrl}/messagejob/all`, {
      method: "get",
      headers: new Headers({
        "content-type": "application/json;charset=UTF-8",
        accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
      })
    })
      .then(response => response.json())
      .then(res =>
        this.setState({
          isLoading: false,
          fieldsData: [res]
        })
      )
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  }

  componentDidMount() {
    this.fetchJobs();
  }

  openModal = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  removeField = index => {
    const { fieldsData } = this.state;

    this.setState({
      fieldsData: fieldsData.filter((field, i) => {
        return i !== index;
      })
    });
  };

  render() {
    const { fieldsData, isLoading, error } = this.state;

    return (
      <Fragment>
        <div className="animated fadeIn">
          <Row>
            <Col xs="12" lg="12">
              <Card>
                <CardHeader>
                  <i className="cui-task icons" /> Tasks Assigned
                  <TableDesc className="ml-auto" />
                </CardHeader>
                <CardBody isloading={isLoading.toString()}>
                  {error ? (
                    <div className="card-text">{error.message}</div>
                  ) : null}
                  {!isLoading ? (
                    <Fragment>
                      <Table responsive bordered hover>
                        <TableHeader />
                        <TasksTable
                          fieldsData={fieldsData}
                          removeField={this.removeField}
                          viewField={this.viewField}
                        />
                      </Table>
                      <Pagination>
                        <PaginationItem>
                          <PaginationLink previous tag="button" />
                        </PaginationItem>
                        <PaginationItem active>
                          <PaginationLink tag="button">1</PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                          <PaginationLink tag="button">2</PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                          <PaginationLink tag="button">3</PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                          <PaginationLink tag="button">4</PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                          <PaginationLink next tag="button" />
                        </PaginationItem>
                      </Pagination>
                    </Fragment>
                  ) : (
                    <Fragment>
                      <div className="text-center">
                        <h2>Fetching messages...</h2>
                      </div>
                      <LoadingOverlay
                        active={true}
                        styles={{
                          overlay: base => ({
                            ...base,
                            background: "rgba(40, 250, 211, 0.5)",
                            animation: "grow",
                            role: "status"
                          }),
                          wrapper: {
                            width: "100px",
                            height: "100px"
                          }
                        }}
                        spinner={
                          <Spinner
                            size={230}
                            spinnerColor={"#28befa"}
                            spinnerWidth={60}
                            visible
                          >
                            <span className="sr-only">Loading...</span>
                          </Spinner>
                        }
                      />
                    </Fragment>
                  )}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </Fragment>
    );
  }
}

export default Tasks;
